from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class GitHubPage:

    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--kiosk")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.driver.implicitly_wait(10)
        self.driver.get("https://github.com")

    def search_main_page(self):
        # self.driver.find_element_by_css_selector(".header-search-input").send_keys("selenium", Keys.ENTER)
        self.driver.get("https://github.com/search?q=Selenium")
        print("Search query")
        self.Measure_web_time()

    def get_curr_url(self):
        return (self.driver.current_url)

    def valid_link(self):
        if "404" in self.get_curr_url():
            print("link is un valid")
        print("link is valid")

    def move_next(self):
        # self.driver.find_element_by_css_selector(".next_page").click()
        self.driver.execute_script("document.getElementsByClassName('next_page')[0].click()")
        print("Move To Next Page")
        self.Measure_web_time()

    def Measure_web_time(self):
        navigationStart = self.driver.execute_script("return window.performance.timing.navigationStart")
        responseStart = self.driver.execute_script("return window.performance.timing.responseStart")
        domComplete = self.driver.execute_script("return window.performance.timing.domComplete")
        backendPerformance = responseStart - navigationStart
        frontendPerformance = domComplete - responseStart
        print("Back End (miliseconds): %s" % backendPerformance)
        print("Front End (miliseconds): %s" % frontendPerformance)

    def kill_browser(self):
        self.driver.quit()
