# Use an official Python runtime as a parent image
FROM python:3.6-alpine3.7

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app


# update apk repo
RUN echo "http://dl-4.alpinelinux.org/alpine/v3.7/main" >> /etc/apk/repositories && \
    echo "http://dl-4.alpinelinux.org/alpine/v3.7/community" >> /etc/apk/repositories

# install chromedriver
RUN apk update
RUN apk add chromium chromium-chromedriver

# install selenium
RUN pip install --trusted-host pypi.python.org -r requirements.txt
RUN pip install selenium==3.8.0

# Run app.py when the container launches
CMD ["python", "git_scrping.py"]
