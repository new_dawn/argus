from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
from pymongo import MongoClient
from main_page import GitHubPage


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors.
    This function just prints them, but you can
    This function just prints them, but you can
    make it do anything.
    """
    print(e)


def getName(item):
    return getText(item, 'a.v-align-middle')


def getStars(item):
    return getText(item, '.pl-2 a.muted-link')


def getDiscription(item):
    return getText(item, '.col-12.col-md-9.d-inline-block.text-gray.mb-2.pr-4')


def getTime(item):
    return getText(item, 'relative-time')


def getLanguage(item):
    return getText(item, '.text-gray.flex-auto.min-width-0')


def getTags(item):
    try:
        return ", ".join([i.text.strip() for i in item.select('.col-12.topics-row-container a')])
    except IndexError as e:
        print(e)
        return ''


def getText(item, seletor):
    try:
        return item.select(seletor)[0].text.strip()
    except IndexError as e:
        print(e)
        return ''


def openDB_and_write(list):
    client = MongoClient('mongo:27017')
    mydb = client["mydatabase"]
    mycol = mydb["selenium_on_git"]
    x = mycol.insert_many(list)
    print(x.inserted_ids)

def get_all_info_pr_item(item):
    name = getName(item)
    stars = getStars(item)
    description = getDiscription(item)
    time = getTime(item)
    language = getLanguage(item)
    tags = getTags(item)
    dict_info = {"name": name, "description": description, "tags": tags, "time": time, "language": language,
            "stars": stars}
    print(dict_info)
    return dict_info

def run():
    githubPage = GitHubPage()
    githubPage.search_main_page()
    allSearchResult=[]
    for i in range(0, 4):
        raw_html = simple_get(githubPage.get_curr_url())
        html = BeautifulSoup(raw_html, 'html.parser')
        githubPage.valid_link()
        for item in html.select('.repo-list-item'):
            dict_info = get_all_info_pr_item(item)
            print("-------------------------------------------------------")
            allSearchResult.append(dict_info)

        githubPage.move_next()
    githubPage.kill_browser()
    openDB_and_write(allSearchResult)

# this is a another idea who to do this, this one is not involving selenium
def run2():
    allSearchResult = []
    for i in range(0, 4):
        raw_html = simple_get("https://github.com/search?q=Selenium&p={0}".format(i))
        html = BeautifulSoup(raw_html, 'html.parser')
        for item in html.select('.repo-list-item'):
            dict_info = get_all_info_pr_item(item)
            print("-------------------------------------------------------")
            allSearchResult.append(dict_info)
    openDB_and_write(allSearchResult)

if __name__ == '__main__':
    run()
