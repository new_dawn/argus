# README #


### What is this repository for? ###

* this repo scrap https://github.com using python BeautifulSoup and selenium
   take search results of first 5 pages and write them to mongodb 

### How do I get set up? ###

* install docker 
* install docker-compose 
* open terminal and run sudo docker build --tag=pythondocker .
* open terminal and run docker-compose up


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

###  personal comments  ###

* There was an issue with chromedriver and chromheadless,
 when working in that state selenium had a problem 
clicking elements and sending keys 
you will see in code in comments original code using selenium web driver methods 
and not in comments how i overcame this issues .\
I could run only the mongodb on docker and make the py script run locally (on local host ) 
in that way i could run using **chrome.exe** and should not have to use the chromedriver --headless flag.
\
I thought it's more important to run everything in dockers, so this task can run easily 
regardless one's OS and local env variables and settings .   
You can see here chrome in headless mode have a known issues 
https://bugs.chromium.org/p/chromedriver/issues/detail?id=1772

* I'm sure if I kept long enough, I'd find a way to solve it.

### Who do I talk to? ###

* Mor Natan
* Mor.natan10@gmail.com
0507421142